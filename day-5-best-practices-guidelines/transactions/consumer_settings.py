# import libraries
import os
from dotenv import load_dotenv

# get env
load_dotenv()

# load variables
kafka_bootstrap_server = os.getenv("KAFKA_BOOTSTRAP_SERVER")
consumer_group_id = os.getenv("KAFKA_CONSUMER_GROUP_ID")


# [json] = consumer config
def consumer_settings_json(broker):

    json = {
        "bootstrap.servers": broker,
        "group.id": consumer_group_id,
        "isolation.level": "read_committed",
        "auto.offset.reset": "earliest",
        "enable.auto.commit": False,
        "enable.partition.eof": True
        }

    # return data
    return dict(json)