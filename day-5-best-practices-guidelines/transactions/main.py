# import libraries
import argparse
import sys
import os
from dotenv import load_dotenv
from eos_producer import EOSKafkaProducer

# get env
load_dotenv()

# load variables
kafka_broker = os.getenv("KAFKA_BOOTSTRAP_SERVER")
kafka_topic = os.getenv("KAFKA_SRC_TOPIC")

# main
if __name__ == '__main__':

    # instantiate arg parse
    parser = argparse.ArgumentParser(description='python application for ingesting data & events into a data store')

    # add parameters to arg parse
    parser.add_argument('entity', type=str, choices=['eos-producer-users-transactions'], help='entities')

    # invoke help if null
    args = parser.parse_args(args=None if sys.argv[1:] else ['--help'])

    # calling [classes]
    if sys.argv[1] == 'eos-producer-users-transactions':
        EOSKafkaProducer().json_eos_producer(broker=kafka_broker, kafka_topic=kafka_topic, get_dt_rows=1)