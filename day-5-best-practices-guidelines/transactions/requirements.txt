python-dotenv==0.19.0
confluent-kafka==1.7.0
pandas==1.3.1
faker==8.11.0