#### Message ordering ~ Apache Kafka

Apache Kafka guarantee message ordering based on partition level, or we can apply options in our producer to deliver the right order of the message.

A few things to understand:
* when we thinking about ordering data we can do in producer level or with only 1 partition
* working with one partition disable the possibility scale write and read because we can improve using more partitions
* a good way is to work in producer level, in this case we can scale and guarantee ordering using exactly once semantics.




```sh
# create topic with 1 partition and with 2 or more partition, in this case ordering happen only in 1 partition because we don't have to split the data across partitions.
k exec -ti edh-kafka-0 -- bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --partitions 2 --topic src-app-message-ordering-json
k exec -ti edh-kafka-0 -- bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --partitions 1 --topic src-app-message-ordering-json

# list topics
kubectl exec edh-kafka-0 -c kafka -i -t -- bin/kafka-topics.sh --bootstrap-server localhost:9092 --list

# consume using consume command line [in case don't want to working with the consumer]

kubectl exec edh-kafka-0 -c kafka -i -t -- \
bin/kafka-console-consumer.sh \
--bootstrap-server localhost:9092 \
--topic src-app-message-ordering-json


```


in this folder we have 1 application with simple kafka producer and a simple kafka consumer, we can see when I enable two producer properties we can enable ordering guarantee.

producer_settings.py
```sh
# enable idempotence give ability to guarantee ordering based on exactly once,  but not only this
# besides enable idempotence we have to declare acks = all, now we have exactly once semantics working in the api
   "enable.idempotence": "true",
   "acks": "all",
```

In consumer side we can see how the messages behave.
Another detail is we don't need the key to guarantee ordering, the ordering is based in the offset and the producer configuration.
