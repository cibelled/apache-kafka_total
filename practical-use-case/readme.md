### Data Engineer with Apache Kafka [Use-case]

#### Instructions:

First of all, congratulations in finish the kafka course, was really good have you with us, but now is time to for you to measure your knowledge learned in our training, so lets do this!
Challenge will measure your knowledge and understanding of Big Data, how properly use kafka in data pipelines and you will be free to do as you like, the only wrong answer is not do it, always test new possibilities!

##### Notes:
* Explore a real use-case and apply your knowledge in coding.
* Best practices of development and creation of stream data pipeline is mandatory.
* always create your codes well documented, with a readme.md file explain how it works

##### Deliverables:
Create a end to end pipeline with kafka using the knowledge you acquire:
* 2 code for producer API
* 2 code for queries in KSQLDB
* 2 code for consumer API
* 4 files for kafka connectors sink and source

All data information are fictional as the use cases and business rules, idea is to create a test environment to apply the knowledge similar to real life.

#### Important notes:

You can choose one or more business segments to apply in your pipeline can be:
* you can use python lib faker or databases such adventure works
* create a ingestion, processing and serving process
* use the examples in this repo to build your code
* create a architecture draw first for what you want to achieve
