### Data Engineer with Apache Kafka [Challenge] ~ Use case


### Deployment

Deployment must be in kubernetes (minikube) using day-1-fundamentals-internals/deployment-options/kubernetes/strimzi

##### Toolkit
* Strimzi Operator
* Kafka Broker - Ephemeral
* Zookeeper
* Kafka Connect
* Schema Registry
* KsqlDB

Don't forget to create a load balancer (if is using minikube command minikube tunnel), this will be use for producer API e consumer API.

###### All demonstrations in the previous class can be use as template.

### ingestion

#### Producer API
* Create a producer API in JSON and other AVRO, any programmming language, to write data into kafka name of topic must follow this pattern, example: src-app-topic-name-json / src-app-topic-name-avro, acks=all, idempotent=true


#### Kafka Connect - Source
* create a source connector using JDBC for one table, using json and another using avro , to write data into kafka name of topic must follow this pattern, example: src-sqlserver-topic-name-json / src-sqlserver-topic-name-avro, don't forget must have key

### Processing
#### KsqlDB - Processing
* create a stream using the topic json and another stream in avro from kafka connector JDBC topics
* create a table from json stream and another avro stream based on aggregation, creating a output topic with this pattern, example: output-stream-topic-name-json / output-stream-topic-name-json
* create stream join between avro stream and json stream from topic based on topic key

### Serving
#### Kafka Connect - Sink
* create a sink connector using JDBC output data from kafka using json and avro topics, can be to postgresql database or mysql database, create with a primary key if needed

#### Consumer API
* create a consumer API to read json and another for avro topics, any programmming language, must be reading for the latest offset
