# get [env] information
BOOTSTRAP_SERVERS = '52.251.13.1:9094'
INPUT_USERS_TOPIC = 'src-app-users-json'
INPUT_MOVIES_TITLES_TOPIC = 'src-app-movies-titles-json'
OUTPUT_TOPIC_COUNTS_PER_COUNTRY_BATCH = 'output-pyspark-counts-country-batch-json'
OUTPUT_TOPIC_COUNTS_PER_GENRES_STREAM = 'output-pyspark-counts-genres-stream-json'