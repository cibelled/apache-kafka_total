#### create objects in ksqldb

For interact with kafka topics using ksqldb is needed create objects stream or tables to make queries.
* ksqldb docs = https://ksqldb.io/

```sh

# access ksqldb server using k8s
KSQLDB=ksqldb-server-5dbf5c69bb-j7dkv
k exec $KSQLDB -n processing -i -t -- bash ksql

# set latest or earliest offset read
SET 'auto.offset.reset' = 'earliest';
SET 'auto.offset.reset' = 'latest';

# read events from TOPICS
PRINT 'src-app-musics-avro' FROM BEGINNING;
PRINT 'src-app-credit-card-avro' FROM BEGINNING;
PRINT 'src-app-agent-avro' FROM BEGINNING;
PRINT 'src-app-users-avro' FROM BEGINNING;
PRINT 'output-ksqldb-stream-pr-musics-analysis-avro' FROM BEGINNING;
PRINT 'output-ksqldb-stream-pr-credit-card-commerce-analysis-avro' FROM BEGINNING;

# avro streams applications
CREATE OR REPLACE STREAM ksql_stream_app_musics_avro WITH (KAFKA_TOPIC='src-app-musics-avro', VALUE_FORMAT='AVRO', KEY_FORMAT = 'AVRO');
CREATE OR REPLACE STREAM ksql_stream_app_credit_card_avro WITH (KAFKA_TOPIC='src-app-credit-card-avro', VALUE_FORMAT='AVRO', KEY_FORMAT = 'AVRO');
CREATE OR REPLACE STREAM ksql_stream_app_agent_avro WITH (KAFKA_TOPIC='src-app-agent-avro', VALUE_FORMAT='AVRO', KEY_FORMAT = 'AVRO');
CREATE OR REPLACE STREAM ksql_stream_app_users_avro WITH (KAFKA_TOPIC='src-app-users-avro', VALUE_FORMAT='AVRO', KEY_FORMAT = 'AVRO');

# avro streams connect source
CREATE OR REPLACE STREAM ksql_stream_sqlserver_subscription_avro WITH (KAFKA_TOPIC='src-sqlserver-subscription-avro', VALUE_FORMAT='AVRO', KEY_FORMAT = 'AVRO');
CREATE OR REPLACE STREAM ksql_stream_sqlserver_credit_card_avro WITH (KAFKA_TOPIC='src-sqlserver-credit-card-avro', VALUE_FORMAT='AVRO', KEY_FORMAT = 'AVRO');
CREATE OR REPLACE STREAM ksql_stream_mysql_commerce_avro WITH (KAFKA_TOPIC='owshq-mysql.owshq.commerce', VALUE_FORMAT='AVRO', KEY_FORMAT = 'AVRO');
CREATE OR REPLACE STREAM ksql_stream_mysql_device_avro WITH (KAFKA_TOPIC='owshq-mysql.owshq.device', VALUE_FORMAT='AVRO', KEY_FORMAT = 'AVRO');

# avro table connect source
CREATE OR REPLACE TABLE ksql_table_sqlserver_bank_avro WITH (KAFKA_TOPIC='src-sqlserver-bank-avro', VALUE_FORMAT='AVRO', KEY_FORMAT = 'AVRO');

# json streams applications
# movies ratings
CREATE OR REPLACE STREAM ksql_stream_app_movies_ratings_json
(
"user_id" INT,
"movieid" INT,
"rating" DOUBLE,
"timestamp" INT,
"dt_current_timestamp" VARCHAR
)
WITH (KAFKA_TOPIC='src-app-movies-ratings-json', VALUE_FORMAT='JSON');


# movies titles
CREATE OR REPLACE STREAM ksql_stream_app_movies_titles_json
(
"user_id" INT,
"adult" VARCHAR,
"belongs_to_collection" VARCHAR,
"genres" VARCHAR,
"id" INT,
"imdb_id" VARCHAR,
"original_language" VARCHAR,
"original_title" VARCHAR,
"overview" VARCHAR,
"popularity" VARCHAR,
"production_companies" VARCHAR,
"production_countries" VARCHAR,
"release_date" VARCHAR,
"revenue" double,
"status" VARCHAR,
"title" VARCHAR,
"vote_average" double,
"vote_count" double,
"dt_current_timestamp" VARCHAR
)
WITH (KAFKA_TOPIC='src-app-movies-titles-json', VALUE_FORMAT='JSON');


# json streams databases
# food postgresql
CREATE OR REPLACE STREAM ksql_stream_postgres_food_json
(
"payload" STRUCT <"type" VARCHAR,
                  "incr" int,
                  "id" int,
                  "uid" VARCHAR,
                  "dish" VARCHAR,
                  "description" VARCHAR,
                  "ingredient" VARCHAR,
                  "measurement" VARCHAR,
                  "user_id" int,
                  "dt_current_timestamp" int,
                  "messagetopic" VARCHAR,
                  "messagesource" VARCHAR>
)
WITH (KAFKA_TOPIC='src-postgres-food-json', VALUE_FORMAT='JSON', KEY_FORMAT = 'JSON');

# create table with aggretation
CREATE OR REPLACE TABLE ksqldb_table_pr_count_bank_avro
WITH (KAFKA_TOPIC='ksqldb-table-pr-count-bank-avro', PARTITIONS=9, VALUE_FORMAT='AVRO')
AS
SELECT
      "BANK_NAME",
      count("BANK_NAME") AS BANK_NAME_COUNT
FROM KSQL_TABLE_SQLSERVER_BANK_AVRO
GROUP BY "BANK_NAME"
emit changes;

```
