"""
filename = config.py
name = configuration

description:

this file is responsible to retain all the metadata for this project. the variables
are available for all .py files.
"""

# file location
music_file_location = 'music_data.csv'

# avro schema definition
key_schema_str = """
{
    "namespace": "my.music_streaming_app",
    "name": "key",
    "type": "record",
    "fields" : [
    {
        "name" : "user_id",
        "type" : "int"
    }
            ]
}
"""

value_schema_str = """
{
    "namespace": "my.music_streaming_app",
    "name": "value",
    "type": "record",
    "fields" : [
    {
        "name" : "user_id",
        "type" : "int"
    },
    {
        "name" : "genre",
        "type" : "string"
    },
    {
        "name" : "artist_name",
        "type" : "string"
    },
    {
        "name" : "track_name",
        "type" : "string"
    },
    {
        "name" : "track_id",
        "type" : "string"
    },
    {
        "name" : "popularity",
        "type" : "int"
    },
    {
        "name" : "duration_ms",
        "type" : "int"
    }
        ]
}
"""
