"""
filename: read_files.py
name: read_files

description:
this is the function responsible to read files and perform some enhancements on the data.
use pandas to read data from a csv file and format to a dictionary
"""

# import libraries
import pandas as pd
import numpy as np
import config

# pandas config
pd.set_option('display.max_rows', 100000)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)


class CSV:

    def __init__(self):
        self.user_file_location = config.user_file_location

    def csv_reader(self, gen_dt_rows):

        # reading file
        get_user_data = pd.read_csv(self.user_file_location)

        # fixing column names
        get_user_data.columns = get_user_data.columns.str.strip().str.lower().str.replace(' ', '_').str.replace('(','').str.replace(')', '')

        # replace [nan] to [none]
        get_user_data = get_user_data.replace({np.nan: None})

        # select column ordering
        user_output = get_user_data[
            [
                'user_id',
                'gender',
                'language',
                'race',
                'job_title',
                'city',
                'country',
                'currency',
                'currency_mode',
                'credit_card_type',
                'subscription_price',
                'time',
                'datetime'
            ]].head(gen_dt_rows)

        # convert to list
        user_list = user_output.to_dict('records')

        return user_list

